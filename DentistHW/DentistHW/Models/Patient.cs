﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DentistHW.Enums;
using DentistHW.Interfaces;

namespace DentistHW.Models
{
    public class Patient : IEntity<int>
    {
        public int Id { get; set; }

        [Required, ForeignKey("Organization")]
        public int OrganizationId { get; set; }

        [Required, StringLength(40)]
        public string FirstName { get; set; }

        [Required, StringLength(40)]
        public string LastName { get; set; }

        [Required, StringLength(40)]
        public string Patronymic { get; set; }

        [Required]
        public DateTime DateOfBirth { get; set; }

        [Required]
        public Sex Sex { get; set; }

        [Required]
        public DateTime CreatedOn { get; set; }

        public Organization Organization { get; set; }
    }
}
