﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DentistHW.Enums;
using DentistHW.Interfaces;

namespace DentistHW.Models
{
    public class Appointment : IEntity<int>
    {
        public int Id { get; set; }

        [Required, ForeignKey("Patient")]
        public int PatientId { get; set; }

        [Required]
        public AppointmentType Type { get; set; }

        [Required]
        public DateTime DateOf { get; set; }

        [Required]
        public AppointmentStatus Status { get; set; }

        [Required]
        public decimal TotalCost { get; set; }

        public Patient Patient { get; set; }
    }
}
