﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DentistHW.Enums
{
    public enum AppointmentStatus
    {
        Planned = 1, OnGoing = 2, Done = 3
    }
}
