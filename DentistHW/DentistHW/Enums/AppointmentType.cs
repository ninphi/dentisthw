﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DentistHW.Enums
{
    public enum AppointmentType
    {
        Examination = 1, Treatment = 2, Supervision = 3
    }
}
